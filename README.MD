# TASK MANAGER

## DEVELOPER INFO

**NAME**: Diana Zolotova

**EMAIL**: dzolotova@t1-consulting.ru

**EMAIL**: orion-copmany@yandex.ru

## SOFTWARE

**JAVA**: JAVA 1.8 OPENJDK

**OS**: WINDOWS 10

## HARDWARE

**CPU**: i5

**RAM**: 16GB

**SSD**: 476GB

## BUILD PROGRAM

```
mvn clean install
```

## RUN PROGRAM

```
java -jar task-manager.jar
```
