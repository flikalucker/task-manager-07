package ru.t1.dzolotova.tm;

import ru.t1.dzolotova.tm.constant.ArgumentConst;
import ru.t1.dzolotova.tm.constant.CommandConst;
import ru.t1.dzolotova.tm.model.Command;
import ru.t1.dzolotova.tm.util.FormatUtil;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        processArguments(args);
        showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:\n");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processArguments(final String[] arg) {
        if (arg == null || arg.length == 0) return;
        for (int i = 0; i < arg.length; i++) {
            runArgument(arg[i]);
        }
        System.exit(0);
    }

    private static void processCommand(final String param) {
        switch (param) {
            case CommandConst.ABOUT:
                showAbout();
                break;
            case CommandConst.VERSION:
                showVersion();
                break;
            case CommandConst.HELP:
                showHelp();
                break;
            case CommandConst.INFO:
                ShowSystemInfo();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                showCommandError();
        }
    }

    private static void runArgument(final String param) {
        switch (param) {
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.INFO:
                ShowSystemInfo();
                break;
            default:
                showArgumentError();
        }
    }

    private static void showWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER**");
    }

    private static void ShowSystemInfo() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + FormatUtil.FormatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = FormatUtil.FormatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = maxMemoryCheck ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String TotalMemoryFormat = FormatUtil.FormatBytes(totalMemory);
        System.out.println("Total memory: " + TotalMemoryFormat);
        final long usageMemory = totalMemory - freeMemory;
        System.out.println("Usage memory: " + FormatUtil.FormatBytes(usageMemory));
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Diana Zolotova");
        System.out.println("email: dzolotova@t1-consulting.ru");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.7.0");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(Command.ABOUT);
        System.out.println(Command.VERSION);
        System.out.println(Command.HELP);
        System.out.println(Command.INFO);
        System.out.println(Command.EXIT);
    }

    private static void showArgumentError() {
        System.out.println("[ERROR]");
        System.out.println("This argument is not supported");
        System.exit(1);
    }

    private static void showCommandError() {
        System.out.println("[ERROR]");
        System.out.println("This command is not supported");
        System.exit(1);
    }

    public static void exit() {
        System.exit(0);
    }

}